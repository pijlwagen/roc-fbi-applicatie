<?php

include '../functions.php';

$page = $_GET['page'] ?? 1;
$perPage = 2;
$indexes = [];

$indexes[0] = $page * $perPage - 2; // 4
$indexes[1] = $page * $perPage; // 6

$result = [];

$queryCount = "SELECT COUNT(id) as total FROM most_wanted;";
$total = $dbh->query($queryCount);
$total->execute();
$total = $total->fetch();

$query = "SELECT * FROM most_wanted LIMIT $perPage OFFSET {$indexes[0]}";


$persons = $dbh->query($query);
$persons->execute();
$result = $persons->fetchAll();

echo json_encode([
    'data' => $result,
    'total' => $total['total']
]);