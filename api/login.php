<?php
include '../functions.php';

if (checkUser()) {
    echo json_encode([
        'is_logged_in' => true
    ]);
    exit;
}

$json = json_decode(file_get_contents('php://input'), true);

if (isset($json['username'])) {
    $username = $json['username'];
    $query = $dbh->query("SELECT * FROM `users` WHERE `name` = \"$username\"");
    $result = $query->fetchAll();

    if (count($result) === 0) {
        echo json_encode([
            'error' => 'User not found.'
        ]);
        exit;
    }

    if (!password_verify($json['password'], $result[0]['password'])) {
        echo json_encode([
            'error' => 'Incorrect password.'
        ]);
        exit;
    }

    $_SESSION['username'] = $result[0]['name'];
    echo json_encode([
        'is_logged_in' => true
    ]);
    exit;
} else {
    echo json_encode([
        'error' => 'Only POST is allowed.'
    ]);
    exit;
}
?>