<?php
include 'functions.php';

if (checkUser()) {
    redirect('index.php');
}

if (isset($_POST['username'])) {
    try {
        if ($_POST['password'] !== $_POST['password_confirm']) {
            redirect('register.php?error=The passwords do not match');
        }

        $username = $_POST['username'];
        $query = $dbh->query("SELECT * FROM `users` WHERE `name` = \"$username\"");
        $result = $query->fetchAll();

        if (count($result) > 0) {
            redirect('register.php?error=Username is already taken');
        }

        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        $query = $dbh->query("INSERT INTO users (name, password) VALUES (\"$username\", \"$password\")");
        $_SESSION['username'] = $username;
        redirect('index.php');
    } catch (Exception $e) {
        var_dump($e);
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://upload.wikimedia.org/wikipedia/commons/d/da/Seal_of_the_Federal_Bureau_of_Investigation.svg">
                    <div class="card-body">
                        <form method="post">
                            <label for="username">Choose your username</label>
                            <input type="text" name="username" class="form-control mb-2">
                            <label for="password">Choose your password</label>
                            <input type="password" name="password" class="form-control mb-2">
                            <label for="password_confirm">Confirm your password</label>
                            <input type="password" name="password_confirm" class="form-control mb-2">
                            <button class="btn btn-primary w-100">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>