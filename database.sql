DROP DATABASE IF EXISTS fbi;

CREATE DATABASE fbi;

USE fbi;

CREATE TABLE most_wanted (id int PRIMARY KEY AUTO_INCREMENT, first_name VARCHAR(255), last_name VARCHAR(255), reward DECIMAL(14,2), notes TEXT);

INSERT INTO most_wanted (first_name, last_name, reward, notes)
VALUES 
  ('John', 'Doe', 50000, 'Bank Robbery - DO NOT APPROACH'),
  ('Jane', 'Smith', 75000, 'Computer Hacking'),
  ('Mike', 'Johnson', 100000, 'Diamond Theft - DO NOT APPROACH');

CREATE TABLE users (id int PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255), password TEXT NOT NULL);

INSERT INTO users (name, password) VALUES ('admin', '$2a$12$0mbXrEvH7pfO9H3tLkrw6uymrUktvx8UvBYf6xNOdvfMesvwtCmv.')