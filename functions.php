<?php
session_start();

$dbh = new PDO('mysql:host=0.0.0.0;dbname=fbi', 'root', 'password');

function checkUser()
{
    return isset($_SESSION['username']);
}

function redirect($where)
{
    header('Location: ' . $where);
    exit;
}
