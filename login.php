<?php
include 'functions.php';

if (checkUser()) {
    redirect('index.php');
}

if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $query = $dbh->query("SELECT * FROM `users` WHERE `name` = \"$username\"");
    $result = $query->fetchAll();

    if (count($result) === 0) {
        redirect('login.php?error=Unknown user');
    }

    if (!password_verify($_POST['password'], $result[0]['password'])) {
        redirect('login.php?error=Incorrect password');
    }

    $_SESSION['username'] = $result[0]['name'];
    redirect('index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://upload.wikimedia.org/wikipedia/commons/d/da/Seal_of_the_Federal_Bureau_of_Investigation.svg">
                    <div class="card-body">
                        <div class="alert alert-danger d-none" id="alert"></div>
                        <label for="username">Gebruikersnaam</label>
                        <input type="text" name="username" id="username" class="form-control mb-2">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control mb-2">
                        <button class="btn btn-primary w-100" onclick="fetchData()">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function fetchData() {
            const username = document.querySelector('#username').value;
            const password = document.querySelector('#password').value;

            fetch(`http://localhost/api/login.php`, {
                method: 'POST',
                body: JSON.stringify({
                    username: username,
                    password: password
                })
            })
                .then(res => res.json())
                .then(data => {
                    if (data.is_logged_in) {
                        window.location = 'index.php';
                    } else if (data.error) {
                        const alert = document.querySelector('#alert');
                        alert.classList.remove('d-none');
                        alert.textContent = data.error;
                    }
                });
        }
    </script>
</body>

</html>