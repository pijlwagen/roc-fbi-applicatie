<?php
include 'functions.php';

if (!checkUser()) {
    redirect('login.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand d-inline-flex" href="#">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Seal_of_the_Federal_Bureau_of_Investigation.svg/300px-Seal_of_the_Federal_Bureau_of_Investigation.svg.png" alt="Logo" width="50">
                <span class="my-auto ms-3">Federal Bureau of Investigation</span>
            </a>
        </div>
    </nav>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="alert alert-success  mb-3">
                    Welcome back <?= $_SESSION['username']; ?>
                </div>

                <h3>Most wanted</h3>
                <div class="card mb-3">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Reward</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

                <nav aria-label="Page navigation example" id='pagination' class='d-none'>
                    <ul class="pagination"></ul>
                </nav>
            </div>
        </div>
    </div>

    <template id='row'>
        <tr>
            <td class='first-name'></td>
            <td class='last-name'></td>
            <td class='reward'></td>
            <td class='notes'></td>
        </tr>
    </template>

    <script>
        function onPageClick(pageNumber) {
            fetchData(pageNumber);
        }

        function fetchData(pageNumber = 1) {
            fetch(`http://localhost/api/most_wanted.php?page=${pageNumber}`)
                .then(res => res.json())
                .then(({ data, total }) => {
                    const pages = Math.ceil(total / 2);
                    const paginationElement = document.querySelector('#pagination');
                    const paginationList = paginationElement.querySelector('ul');
                    paginationList.innerHTML = '';  

                    for (let currentPage = 1; currentPage <= pages; currentPage++) {
                        const listItem = `<a class="page-link" href="#">${currentPage}</a>`;
                        const htmlNode = document.createElement('li');
                        htmlNode.classList.add('page-item');
                        htmlNode.innerHTML = listItem;
                        htmlNode.addEventListener('click', () => {
                            onPageClick(currentPage)
                        });
                        paginationList.appendChild(htmlNode);
                    }
                    if (pages > 0) paginationElement.classList.remove('d-none');

                    const body = document.querySelector('tbody');
                    body.innerHTML = '';
                    for (const person of data) {
                        const clone = document.getElementById('row').content.cloneNode(true);
                        clone.querySelector('.first-name').innerHTML = person.first_name;
                        clone.querySelector('.last-name').innerHTML = person.last_name;
                        clone.querySelector('.reward').innerHTML = person.reward;
                        clone.querySelector('.notes').innerHTML = person.notes;
                        body.appendChild(clone);
                    }
                });
        }

        function search() {
            const id = prompt("Who are you looking for?");
            fetchData(id);
        }
    </script>

    <button class="btn btn-primary" onclick="fetchData()">Data ophalen</button>
    <button class="btn btn-primary" onclick="search()">Gebruiker zoeken</button>
</body>

</html>